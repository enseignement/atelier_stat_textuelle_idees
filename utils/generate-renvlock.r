
library("renv")

repos <- c(CRAN = "https://cloud.r-project.org")

options(renv.settings.r.version = "4.2.2", repos = repos)


renv::restore()

