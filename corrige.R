################################################################################

# CORRIGE DES EXERCICES

################################################################################



#Exercice 1
# ______________________________________________________________________________

# Chargement du tableau et tokenisation (cf. points précédents)
my_tokens <- tokens(sel_text, remove_punct = TRUE, 
                    remove_symbols = TRUE,
                    remove_numbers = TRUE)

docvars(my_tokens) <- text_df

print(my_tokens)




# Exercice 2
# ______________________________________________________________________________

# création de la dfm
JO_dfm <- dfm(JO_tokens)

# filtrage sur les effectifs
JO_dfm <- dfm_trim(JO_dfm, min_termfreq = 5)

# suppression de termes dans la dfm
#JO_dfm<- dfm_select(JO_dfm,pattern=c("chine","pékin","chinois","londres",
#                                     "rio","brésil","cariocas","tokyo",
#                                     "japon"),
#                    selection = "remove")

# création du TLA
JO_dfm_period <- dfm_group(JO_dfm,groups = period)

# analyse des spécificités via texts
textstat_keyness(JO_dfm_period, target="Beinjing2008")

# visu
textstat_keyness(JO_dfm_period, target="Beinjing2008") %>% 
  textplot_keyness(labelsize =2)



# Exercice 3
# ______________________________________________________________________________

# création de la dfm
JO_dfm <- dfm(JO_tokens)

# filtrage sur les effectifs
JO_dfm <- dfm_trim(JO_dfm, min_termfreq = 5)

# suppression de termes dans la dfm
JO_dfm<- dfm_select(JO_dfm,pattern=c("chine","pékin","chinois","londres",
                                     "rio","brésil","cariocas","tokyo",
                                     "japon"),
                    selection = "remove")

# création du TLA
JO_dfm_period <- dfm_group(JO_dfm,groups = period)

# transformation du tableau en matrice
JO_dfm_period_mat <- convert(JO_dfm_period,to="matrix")
# transposition de la matrice pour faire apparaître les modalités en colonnes
JO_dfm_period_mat <- t(JO_dfm_period_mat)

# calcul de l'AFC
afc_JO <- CA(JO_dfm_period_mat, 
             graph = FALSE)

# valeurs propres
fviz_screeplot(afc_JO)

# visualisation du plan avec explor
explor(afc_JO)



# Exercice 4
# _____________________________________________________________________________

# création de la dfm

res.hcpc.JO <- HCPC(afc_JO,metric="manhattan",method="ward",
                    graph = FALSE)

# visualisation du dendrogramme
plot(res.hcpc, choice="tree", cex=0.2)




