---
title: "Untitled"
editor: visual
---

# Introduction

## Interdisciplinarité

"la statistique textuelle, domaine de recherche vivant dont les contours exacts sont difficiles à établir tant est large l'éventail des disciplines concernées, et aussi celui des applications possible" (Lebart et Salem, 1992, p. 5).

## L'héritage de l'analyse de données à la française

Littéralement "mesure des textes".

Analyser un corpus textuel à travers des mesures statistiques effectuées sur le contenu textuel.

Elle correspond à ce que l'on désignait à travers le terme de **lexicométrie** (mesure du lexique), dans les années 1970-80.

[Lebart, L., et, Salem, A., 1994, *Statistique textuelle*, Paris, Dunod, 342 p .](http://lexicometrica.univ-paris3.fr/livre/st94/st94-tdm.html)

## Des méthodes et des logiciels canoniques


### Méthode de l'analyse de données

- Des méthodes bien étalonnées construction de tableaux, test statitiques (avec le couple afc / cah)

- Pensé pour l'utilisateur (facilité d'interprétation des résultats)



### Les logiciels


**Beaudouin, 2016**:

**En bref l'adoption des théories passe par les logiciels qui se reconfigurent en fonction des usages. Ces logiciels cristallisent d'une part les débats théoriques au sein de la communauté et d'autre part posent la question des enjeux économiques, ou plus modestement commerciaux, liés à ces méthodes.** En effet, la diffusion de ces méthodes a été soutenue par des enjeux économiques : dans le secteur des études et du marketing, la possibilité de faire du quantitatif sur des données qualitatives, autrement dit la possibilité d'introduire de la mesure dans l'analyse du discours a représenté une opportunité intéressante.

Deux exemples mentionnés: SPAD-T et Alceste

### La statistique textuelle dans R

- **Gratuité** :  possibilité de reproduire des analyses disponibles dans des logiciels historiques de la statistique textuelle souvent payants (Alceste, module d'analyse lexical de SPAD) 

- possibilités de répétabilité  d'automatisation d'opérations existantes dans d'autres logiciels d'analyse textuelle (cas de TXM).

- potentiellement de reproductibilité

- interfaçage avec d'autres modules, réduire les problèmes de ruptures de charge

- des communautés d'intérêt

### Choix de l'outil: quanteda

**R** via le package **quanteda** (Watanabe et Müller, 2023).

D'autres possibilités (Tidy text, R.temis)

## Objectifs

Des rappels théoriques et des applications sur des corpus d'exemple seront proposées sur les méthodes suivantes : 

• La transformation de textes (chaînes de caractères) en tableau (tableau lexical entier : TLE), 

• Donc la modélisation du tableau de données (définition des unités de contexte et des unités lexicales, sélection des termes et autres formes de « nettoyage » des données textuelles), 

• L'analyse du lexique et du concordancier, • L'analyse du vocabulaire spécifique entre des sous-corpus (construction de tableaux lexicaux agrégés -- TLA et test du chi2), 

• L'analyse des correspondances (AFC) appliquée à un TLA, 

• L'analyse des cooccurrences et des segments répétés, 

• Selon le temps disponible : méthodes de classification de texte (CAH et/ou CDH de Reinert -- méthode historiquement utilisée dans Alceste et Iramutec -- via le package rainette -- Barnier, 2023).

## Plan de la séance:

Descriptif des journées :

La première journée sera consacrée à la manipulation des fonctions de quanteda sur les corpus d'exercice à partir de données de presse et de résultats d'enquêtes (réponses à des questions ouvertes dans un questionnaire d'enquête).

Le lendemain, une demi-journée sera proposée aux collègues souhaitant appliquer ces méthodes à leur propres corpus. Pour ce faire, il est préférable de consulter le didacticiel de quanteda concernant l'import des données : https://tutorials.quanteda.io/import-data/ . Un exemple de formatage de données sous la forme d'un fichier .csv est proposé via ce lien.




